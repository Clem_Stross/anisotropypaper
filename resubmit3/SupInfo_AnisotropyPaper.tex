\documentclass[journal=jpcbfk,manuscript=article]{achemso}


\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{fancyhdr}
\usepackage{braket}
%\usepackage{floatrow}
\usepackage{wrapfig}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{commath}
\usepackage[usenames,dvipsnames]{color}
\usepackage{sidecap}




\renewcommand\vec\mathbf
\SectionNumbersOn



\author{Clement Stross }
\affiliation{Centre for Computational Chemistry, School of Chemistry, University of Bristol, Bristol BS8 1TH, UK}
\alsoaffiliation{School of Mathematics, University of Bristol, Bristol BS8 1TW, UK}

\author{Marc W. Van der Kamp}
\affiliation{Centre for Computational Chemistry, School of Chemistry, University of Bristol, Bristol BS8 1TH, UK}

\author{Thomas A. A. Oliver}
\affiliation{School of Chemistry, University of Bristol, Bristol BS8 1TH, UK}

\author{Jeremy Harvey}
\affiliation{Centre for Computational Chemistry, School of Chemistry, University of Bristol, Bristol BS8 1TH, UK}
\alsoaffiliation{Department of Chemistry, KU Leuven, Celestijnenlaan 200F, B-3001 Leuven}

\author{Noah Linden}
\affiliation{School of Mathematics, University of Bristol, Bristol BS8 1TW, UK}

\author{Frederick R. Manby}
\affiliation{Centre for Computational Chemistry, School of Chemistry, University of Bristol, Bristol BS8 1TH, UK}
\email{fred.manby@bristol.ac.uk}
\phone{}


%\title{Exciton depolarisation in the purple bacteria light harvesting complex LHII}
\title{How static disorder mimics decoherence in anisotropy pump-probe experiments on purple-bacteria light harvesting complexes \\ Supplementary Information}


\begin{document}


\begin{suppinfo}
\section{Molecular dynamics}

We compute ensemble averages for comparison with experiment based on the structures taken as snapshots from Molecular Dynamics (MD) simulations. Each LHII configuration will have a different Hamiltonian, and therefore different exciton dynamics. The starting point for MD simulations was PDB-entry 2FKW \cite{Cherezov2006}, with the lauryl dimethylamine-N-oxide (LDA) molecules removed. For all molecular mechanics and molecular dynamics calculations the program AMBER12 was used \cite{AMBER}. A set of standard protein force fields and topologies is distributed with AMBER12 and ff12SB was used here. LHII, however, contains many non-standard protein components, including the bacteriochlorophyll molecules (BChl) and the carotenoid RG1. Chlorophyll parameters developed by Ceccarelli et al. were used \cite{Ceccarelli2003}. The RG1 molecule can be thought of in two sections, a sugar head group, and a conjugated tail. For both these sections, molecular mechanics parameters have been put forward previously \cite{GLYCAM06,CatTop}. The sugar head group was formed using the atom types in reference \cite{GLYCAM06}. The conjugated tail in RG1 is identical to that found in $\beta$-carotene, so the atom types and charges for the conjugated chain were taken directly from works mapping the exact topology of $\beta$-carotene \cite{CatTop}. For this method the charges were a combination of AM1-BCC charges calculated by the ANTECHAMBER \cite{ANTECHAMBER,GAFF} program for the chain (including the link to the sugar head group) and those from the GLYCAM06 \cite{GLYCAM06} parameters for the sugar head group.

LHII contains a methionine with a carboxylated N-terminus. This nonstandard residue has been named CXM. It is situated close to each of the nine Mg atoms in the BChl of the B800 ring. The parameters for CXM residue were based on those from a MET (Methionine) residue in the AMBER ff12SB force field. The charges on the NH-COO$^{-}$ group were manually adjusted from standard backbone NH and carboxylate groups in other residues, to get overall charge of $-$1 once deprotonation is included.

Histidine residues have two possible neutral tautomers of similar free energies. The tautomer (and protonation state) of any given histidine residue depends on the local environment. Here, all histidines were assumed to be neutral (based on the environment). The tautomers were chosen by determining the optimal hydrogen bonding network using the online WHAT-IF web server \cite{HisPos1996,WhatIf}. This determines which hydrogen bonds are most likely to be formed, and thereby which histidine tautomers are most likely to be present. For the histidines that coordinate directly to the Mg in BChl (with NE2), ND1 was protonated. In addition, a mild restraint was applied (force constant of $10$ kcal mol$^{-1}$  \AA$^{-2}$) to maintain the histidine coordination to the BChl Mg, only when the distance between NE2 and Mg was greater than $3.0$ \AA.

The molecular dynamics simulations were run with a generalized Born implicit solvent model \cite{MDSetUp}, without a cut-off for non-bonded interactions. 
The NVT ensemble was employed, using Langevin dynamics for temperature control and a 2 fs time step (with SHAKE applied). After brief minimization, the system was heated and equilibrated at 300 K (for 200 ps). Thereafter, production simulations were performed with two different sets of conditions; with dielectric constant $\varepsilon$ set to that of either water (78.5) or a lipid environment (10). These simulations gave little to no difference in the relative BChl dynamics. In this work we used the geometries from the aqueous run. Geometries were saved at 0.5ps intervals for 38ns, which gave a ensemble of uncorrelated snapshots. Random snapshots from this ensemble were used in subsequent calculations.

\end{suppinfo}


%\bibliography{Clem-AnisotropyPaper.bib}
%\bibliography{achemso-demo}

\providecommand{\latin}[1]{#1}
\providecommand*\mcitethebibliography{\thebibliography}
\csname @ifundefined\endcsname{endmcitethebibliography}
  {\let\endmcitethebibliography\endthebibliography}{}
\begin{mcitethebibliography}{11}
\providecommand*\natexlab[1]{#1}
\providecommand*\mciteSetBstSublistMode[1]{}
\providecommand*\mciteSetBstMaxWidthForm[2]{}
\providecommand*\mciteBstWouldAddEndPuncttrue
  {\def\EndOfBibitem{\unskip.}}
\providecommand*\mciteBstWouldAddEndPunctfalse
  {\let\EndOfBibitem\relax}
\providecommand*\mciteSetBstMidEndSepPunct[3]{}
\providecommand*\mciteSetBstSublistLabelBeginEnd[3]{}
\providecommand*\EndOfBibitem{}
\mciteSetBstSublistMode{f}
\mciteSetBstMaxWidthForm{subitem}{(\alph{mcitesubitemcount})}
\mciteSetBstSublistLabelBeginEnd
  {\mcitemaxwidthsubitemform\space}
  {\relax}
  {\relax}

\bibitem[Cherezov \latin{et~al.}(2006)Cherezov, Clogston, Papiz, and
  Caffrey]{Cherezov2006}
Cherezov,~V.; Clogston,~J.; Papiz,~M.~Z.; Caffrey,~M. {Room to Move:
  Crystallizing Membrane Proteins in Swollen Lipidic Mesophases}. \emph{J. Mol.
  Biol.} \textbf{2006}, \emph{357}, 1605--1618\relax
\mciteBstWouldAddEndPuncttrue
\mciteSetBstMidEndSepPunct{\mcitedefaultmidpunct}
{\mcitedefaultendpunct}{\mcitedefaultseppunct}\relax
\EndOfBibitem
\bibitem[Case \latin{et~al.}(2012)Case, Darden, Cheatham, Simmerling, Wang,
  Duke, Luo, Walker, Zhang, Merz, Roberts, Hayik, Roitberg, Seabra, Swails,
  Goetz, Kolossv{\'{a}}ry, Wong, Paesani, Vanicek, Wolf, Liu, Wu, Brozell,
  Steinbrecher, Gohlke, Cai, Ye, Wang, Hsieh, Cui, Roe, Mathews, Seetin,
  Salomon-Ferrer, Sagui, Babin, Luchko, Gusarov, Kovalenko, and Kollman]{AMBER}
Case,~D.~A.; Darden,~T.~A.; Cheatham,~T.~E.; Simmerling,~C.~L.; Wang,~J.;
  Duke,~R.~E.; Luo,~R.; Walker,~R.~C.; Zhang,~W.; Merz,~K.~M. \latin{et~al.}
  {AMBER 12}. University of California, 2012\relax
\mciteBstWouldAddEndPuncttrue
\mciteSetBstMidEndSepPunct{\mcitedefaultmidpunct}
{\mcitedefaultendpunct}{\mcitedefaultseppunct}\relax
\EndOfBibitem
\bibitem[Ceccarelli \latin{et~al.}(2003)Ceccarelli, Procacci, and
  Marchi]{Ceccarelli2003}
Ceccarelli,~M.; Procacci,~P.; Marchi,~M. {An Ab Initio Force Field for the
  Cofactors of Bacterial Photosynthesis}. \emph{J. Comput. Chem.}
  \textbf{2003}, \emph{24}, 129--142\relax
\mciteBstWouldAddEndPuncttrue
\mciteSetBstMidEndSepPunct{\mcitedefaultmidpunct}
{\mcitedefaultendpunct}{\mcitedefaultseppunct}\relax
\EndOfBibitem
\bibitem[Kirschner \latin{et~al.}(2008)Kirschner, Yongye, Tschampel,
  Gonz{\'{a}}lez-Outeiri{\~{n}}o, Daniels, Foley, and Woods]{GLYCAM06}
Kirschner,~K.~N.; Yongye,~A.~B.; Tschampel,~S.~M.;
  Gonz{\'{a}}lez-Outeiri{\~{n}}o,~J.; Daniels,~C.~R.; Foley,~B.~L.;
  Woods,~R.~J. {GLYCAM06: A Generalizable Biomolecular Force Field.
  Carbohydrates}. \emph{J. Comput. Chem.} \textbf{2008}, \emph{29},
  622--655\relax
\mciteBstWouldAddEndPuncttrue
\mciteSetBstMidEndSepPunct{\mcitedefaultmidpunct}
{\mcitedefaultendpunct}{\mcitedefaultseppunct}\relax
\EndOfBibitem
\bibitem[Zhang \latin{et~al.}(2012)Zhang, Silva, Yan, and Huang]{CatTop}
Zhang,~L.; Silva,~D.-A.; Yan,~Y.; Huang,~X. {Force Field Development for
  Cofactors in the Photosystem II}. \emph{J. Comput. Chem.} \textbf{2012},
  \emph{33}, 1969--1980\relax
\mciteBstWouldAddEndPuncttrue
\mciteSetBstMidEndSepPunct{\mcitedefaultmidpunct}
{\mcitedefaultendpunct}{\mcitedefaultseppunct}\relax
\EndOfBibitem
\bibitem[Wang \latin{et~al.}(2006)Wang, Wang, Kollman, and Case]{ANTECHAMBER}
Wang,~J.; Wang,~W.; Kollman,~P.~A.; Case,~D.~A. {Automatic Atom Type and Bond
  Type Perception in Molecular Mechanical Calculations}. \emph{J. Mol. Graph.
  Model.} \textbf{2006}, \emph{25}, 247260\relax
\mciteBstWouldAddEndPuncttrue
\mciteSetBstMidEndSepPunct{\mcitedefaultmidpunct}
{\mcitedefaultendpunct}{\mcitedefaultseppunct}\relax
\EndOfBibitem
\bibitem[Wang \latin{et~al.}(2004)Wang, Wolf, Caldwell, and Kollman]{GAFF}
Wang,~J.; Wolf,~R.~M.; Caldwell,~J.~W.; Kollman,~P.~A. Development and Testing
  of a General {AMBER} Force Field. \emph{J. Comput. Chem.} \textbf{2004},
  \emph{25}, 1157--1174\relax
\mciteBstWouldAddEndPuncttrue
\mciteSetBstMidEndSepPunct{\mcitedefaultmidpunct}
{\mcitedefaultendpunct}{\mcitedefaultseppunct}\relax
\EndOfBibitem
\bibitem[Hooft \latin{et~al.}(1996)Hooft, Sander, and Vriend]{HisPos1996}
Hooft,~R.~W.; Sander,~C.; Vriend,~G. {Positioning Hydrogen Atoms by Optimizing
  Hydrogen-Bond Networks in Protein Structures.} \emph{Proteins} \textbf{1996},
  \emph{26}, 363--376\relax
\mciteBstWouldAddEndPuncttrue
\mciteSetBstMidEndSepPunct{\mcitedefaultmidpunct}
{\mcitedefaultendpunct}{\mcitedefaultseppunct}\relax
\EndOfBibitem
\bibitem[Vriend(1990)]{WhatIf}
Vriend,~G. {WHAT IF: A Molecular Modeling and Drug Design Program}. \emph{J.
  Mol. Graph.} \textbf{1990}, \emph{8}, 52--56\relax
\mciteBstWouldAddEndPuncttrue
\mciteSetBstMidEndSepPunct{\mcitedefaultmidpunct}
{\mcitedefaultendpunct}{\mcitedefaultseppunct}\relax
\EndOfBibitem
\bibitem[Onufriev \latin{et~al.}(2004)Onufriev, Bashford, and Case]{MDSetUp}
Onufriev,~A.; Bashford,~D.; Case,~D.~A. {Exploring Protein Native States and
  Large-Scale Conformational Changes with a Modified Generalized Born Model}.
  \emph{Proteins Struct. Funct. Bioinforma.} \textbf{2004}, \emph{55},
  383--394\relax
\mciteBstWouldAddEndPuncttrue
\mciteSetBstMidEndSepPunct{\mcitedefaultmidpunct}
{\mcitedefaultendpunct}{\mcitedefaultseppunct}\relax
\EndOfBibitem
\end{mcitethebibliography}


\end{document}