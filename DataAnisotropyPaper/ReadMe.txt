Absorption Spectra
	The calculated and experimental absorption spectra for LHII
	First column is wavelength in nm, the second column is absorption in arbitrary units
These data are used to produce figure 2


BChl_TDDFT
	The energies and transition dipole moment magnitudes for B850-alpha, B850-beta and B850 BChls.
	These BChls were chosen at random from the pdb files representing the dynamics of the LHII complex.
	The energies and transition dipole moment magnitudes are in atomic units
These data are used to produce table 1
	


LHII_Ensemble
	Data for the absorptions of a random ensemble of LHIIs. Each LHII is a different geometry, and has different site energies and transition dipole moments. For each LHII the pump and probes are chosen randomly (within the parallel and perpendicular constraints)
		Files with the prefix "_All" contain a set of data points for each LHII, files without the "_All" prefix are a average over the ensemble of LHIIs.
		Files with the prefix "_Para" are from parallel pump and probe, files with prefix "_Perp" are perpendicular pump and probe.
	
	TimeVal - Contains time step (dt) in fs, and number of time steps (nt)
	
	A_GSB - Ground state bleaching absorption (single value per LHII as it is time invariant)
	A_SE - Stimulated emission absorption (nt values per LHII)
	A - Total absorption A_GSB+A_SE
	
	TDM_Mmer - The magnitude of transition dipole moment of the LHII ordered by energy (27 values per LHII)
	AbsorpCoff - Contain the values of $\delta_{k}$ (27 per LHII)
	EigenEnergies - The values of the eigenenergies E_{k} (27 values per LHII) (units of a.u.)
	EigenstateDeloc - The inverse partition of the eigenstates of each LHII (27 values per LHII)
	
	Deloc - The inverse partition length (nt values per LHII)
	
	Anisotropy_GSB - The anisotropies from only A_GSB (single value per LHII)
	Anisotropy_SE - The anisotropies from only A_SE (nt values per LHII)
	Anisotropy_All - The full anisotropy using A_GSB+A_SE for each LHII at the chosen pump probe geometry (nt values per LHII)
	Anisotropy - The full anisotropy using A_GSB+A_SE for all LHIIs (nt values per LHII) (sum are performed over absorptions not anisotropies)
These data are used to produce figures 3,5,6,7
	
LHII_Single
	LHII_TDM - Transition dipole moments for LHII eigenstates
	MgPos - Mg positions
These data are used to produce Figure 4
	
